import React, { useState, useEffect } from 'react';
import { MapContainer, TileLayer, Marker, Popup, useMapEvents } from 'react-leaflet';
import axios from 'axios';
import 'leaflet/dist/leaflet.css';

import MapClickHandler from './MapClickHandler';
import CountryInfo from './CountryInfo';

import L from 'leaflet';
import markerIcon from 'leaflet/dist/images/marker-icon.png';
import markerShadow from 'leaflet/dist/images/marker-shadow.png';

delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
  iconRetinaUrl: markerIcon,
  iconUrl: markerIcon,
  shadowUrl: markerShadow,
});

const NORTH_BOUND = 85;
const SOUTH_BOUND = -85;

function Map() {
  const [selectedCountry, setSelectedCountry] = useState(null);
  const [fetchedCountry, setFetchedCountry] = useState(null);
  const [markerPosition, setMarkerPosition] = useState(null);
  const [isFetching, setIsFetching] = useState(false);

  async function fetchCountry(lat, lng) {
    try {
      const response = await axios.get(
        `https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=${lat}&lon=${lng}`
      );
      const country = response.data.address?.country;
      setFetchedCountry(country || 'Unknown');
    } catch (error) {
      console.error(error);
      setFetchedCountry('Unknown');
    }
  }

  async function handleClick(e) {
    const { latlng } = e;
    let { lat, lng } = latlng;

    setIsFetching(true);

    // Adjust the longitude using modulo arithmetic and wrap it within the -180 to 180 range
    let adjustedLng = ((lng - 180) % 360) + 180;
    if (adjustedLng > 180) {
      adjustedLng -= 360;
    }

    try {
      await fetchCountry(lat, adjustedLng);
      setMarkerPosition([lat, lng]);
    } finally {
      setIsFetching(false);
    }
  }

  useEffect(() => {
    if (fetchedCountry) {
      setSelectedCountry(fetchedCountry);
    }
  }, [fetchedCountry]);

  return (
    <div className="w-full h-96">
      <MapContainer
        center={[0, 0]}
        zoom={2}
        style={{ height: '100%' }}
        maxBounds={[
          [SOUTH_BOUND, -Infinity],
          [NORTH_BOUND, Infinity],
        ]}
      >
        <style>
          {`
            .leaflet-bottom.leaflet-right {
              display: none;
            }
          `}
        </style>
        <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
        {markerPosition && (
          <Marker position={markerPosition} interactive={false}>
            <Popup>{selectedCountry}</Popup>
          </Marker>
        )}
        <MapClickHandler onClick={handleClick} />
      </MapContainer>
      {isFetching && <p className="mt-4">Fetching country information...</p>}
      {selectedCountry && <CountryInfo country={selectedCountry} />}
    </div>
  );
}

export default Map;
