import React, { useState } from 'react';
import Map from './Map';

function App() {
  const [selectedCountry, setSelectedCountry] = useState('');

  function handleMapClick(country) {
    setSelectedCountry(country);
  }

  return (
    <div className="flex flex-col items-center justify-center min-h-screen bg-gray-100">
      <h1 className="text-3xl font-bold mb-8">Click on the map to get the country</h1>
      <div className="w-full bg-white rounded-lg shadow-lg p-4">
        <Map onMapClick={handleMapClick} />
      </div>
      {selectedCountry && selectedCountry !== 'Not a country' && <p className="mt-4">Selected Country: {selectedCountry}</p>}
      {selectedCountry === 'Not a country' && <p className="mt-4">Not a valid country</p>}
    </div>
  );
}

export default App;
