import { useMapEvents } from 'react-leaflet';

function MapClickHandler({ onClick }) {
  useMapEvents({
    click: onClick,
  });

  return null;
}

export default MapClickHandler;
