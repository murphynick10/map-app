import React from 'react';

function CountryInfo({ country }) {
  return <p className="mt-4">Selected Country: {country}</p>;
}

export default CountryInfo;
